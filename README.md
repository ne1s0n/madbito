# MADBITO
## The MAD Bioinformatician Toolbox

Madbito is a collection of tools for helping bioinformaticians in common operations, such as reading vcf or big matrix files, filtering SNP matrices or imputing missing values.

## Dependencies

Madbito doesn't require other packages until specific functions are invoked. As such, it only provides a list of suggested packages:

* ggplot2
* scrime
* lme4
* plyr
* doParallel
* missForest

## Installation

```
install.packages("remotes")
library(remotes)
install_gitlab("ne1s0n/madbito")
```
## Documentation
Please see the docs for the complete description of the functions.




