#' Create temporary folder
#'
#' Shortcut to calling tempfile() and then dir.create()
#'
#' @param ... all arguments are passed to tempfile function from {base} package
#' to create a valid path to a temporary folder.
#'
#' @return The name of the created folder.
#'
#' @examples
#' tempdir.create()      #creates a temporary folder with default name from tempdir function
#' tempdir.create('aaa') #created folder will start with 'aaa'
#'
#' @export
tempdir.create = function(...){
  dir = tempfile(...)
  dir.create(dir, showWarnings = FALSE, recursive = TRUE)
  return(dir)
}

#' A publication theme
#'
#' This function returns a set of valid ggplot elements that can be
#' added to an existing plot to bring it to "ready for publication" state.
#'
#' @param remove_axis_titles if TRUE (default) title for axis
#' @return a set of valid ggplot elements
#' @export
ggplot_publication_theme = function(remove_axis_titles=TRUE){
  th = ggplot2::theme(
    text = ggplot2::element_text(size = 30, color = 'black'),
    legend.position = "none",
    panel.grid.minor = ggplot2::element_blank(),
    panel.grid.major = ggplot2::element_blank(),
    panel.background = ggplot2::element_rect(fill = 'white', colour = 'black'),
    axis.text = ggplot2::element_text(colour = 'black') #somehow we need to explicitly assign again the color
  )
  if (remove_axis_titles){
    th = th + ggplot2::theme(
      axis.title.x = ggplot2::element_blank(),
      axis.title.y = ggplot2::element_blank()
    )
  }

  return  (th)
}

#' Rename Single Column (or Named Slot)
#'
#' Function to rename a column of a dataframe or matrix, or
#' a slot name in a named array. Either pos or oldname must
#' be not NULL (but not both).
#'
#' @param x dataframe, matrix or named array
#' @param oldname name of the column (or slot) to be renamed
#' @param pos column (or slot) position to be renamed
#' @param newname the new name
#'
#' @return A copy of x with the renamed column (or slot)
#' @export
#'
#' @examples
#' #renaming a data frame
#' df = data.frame(apple = 1:5, pear = 6:10)
#' df = cbind(df, 11:15)
#' colnames(df)
#'
#' #pear to cherry
#' df = col.rename(df, oldname = 'pear', newname = 'cherry')
#' colnames(df)
#'
#' #third column to banana
#' df = col.rename(df, pos = 3, newname = 'banana')
#' colnames(df)
#'
#' #error: out of bounds
#' \dontrun{
#' df = col.rename(df, pos = 4, newname = 'cabbage')
#' }
#'
#' #same thing with named array
#' a = c(aaa=1, bbb=2)
#' col.rename(a, oldname = 'aaa', newname = 'ccc')
#' col.rename(a, pos = 1, newname = 'ccc')
col.rename = function(x, oldname=NULL, pos=NULL, newname){
  if (!xor(is.null(oldname), is.null(pos))){
    stop('Either oldname or pos must be not NULL (but not both)')
  }

  #are we working on 1D or 2D?
  if (is.null(nrow(x))){
    #single dimension array
    n = names(x)
  }else{
    #matrix-like
    n = colnames(x)
  }

  #looking for the place to tweak
  if (is.null(pos)){
    pos = oldname == n
  }
  n[pos] = newname

  #putting back names
  if (is.null(nrow(x))){
    #single dimension array
    names(x) = n
  }else{
    #matrix-like
    colnames(x) = n
  }

  return (x)
}

#' Parse passed hapmap count (.hmc) file
#'
#' This function parses the passed hapmap count coming from
#' UNEAK pipeline and returns a list of two elements containing
#' the read counts and the reference allele count. Each element
#' is a dataframe (markers x samples)
#'
#' @param filecon a connection to the file to be read and parsed. The header is supposed to be alread read.
#' @param sample.names array of strings, to be used for naming columns of result
#' @param n number of lines to be read
#'
#' @return a list with two elements: $readcnt and $refcnt, each
#'         one a dataframe (markers x samples), or NULL, if nothing more can
#'         be read from \code{filecon}
#' @export
parse_UNEAK_hmc = function(filecon, sample.names, n=1000){
  #reading the file. we have tab separated groups as 2|14, 4|5 and so forth

  #reading everything as string
  df = readLines(filecon, n = n)
  lines.num = length(df)

  #are we at the end of file?
  if (lines.num == 0){
    #let's just return NULL
    return(NULL)
  }

  #substitute pipe with tab
  df = gsub('|', replacement = '\t', x = df, fixed = TRUE)

  #split on tab
  df = do.call(rbind.data.frame,strsplit(df,split="\t", fixed = TRUE))

  #first column is marker names
  markers = as.character(df[,1])
  rownames(df) = markers

  #PROBLEM: as.numeric conversion with have a single line
  #returns a one dimensional object. It creates problems with all the
  #further addressing (things like [,i] won't work anymore).
  #So we duplicate the line, and later remove the duplicate.
  if (lines.num == 1){
    df = rbind(df, df)
  }

  #removing marker names
  df = df[,-1]

  #at this point we have, in df, only the interesting data
  #they are, however, recorded as levels, and we want nice numbers
  df = apply(X = df, MARGIN = 2, FUN = as.numeric)

  #removing statistics columns (last five)
  df = df[,1:(ncol(df)-5)]

  #odd columns are reference allele counts, even columns are variant counts
  odd = seq(from=1, to=ncol(df), by=2)

  ref.cnt = data.frame(df[,odd])
  colnames(ref.cnt) = sample.names

  read.cnt = data.frame(df[,odd] + df[,odd + 1])
  colnames(read.cnt) = sample.names

  #if we added a duplicate line, it's time to remove it
  if (lines.num == 1){
    ref.cnt = ref.cnt[1,]
    read.cnt = read.cnt[1,]
  }

  #adding marker names
  rownames(ref.cnt) = markers
  rownames(read.cnt) = markers

  #and we are done
  return(list(
    'ref.cnt' = ref.cnt,
    'read.cnt' = read.cnt
  ))
}

#' Assignments for stratified crossvalidation
#'
#' This function creates the assignments to perform a
#' stratified crossvalidation. It requires, for each
#' element of the set, a label describing the class it
#' belongs to. It also requires the number of folds.
#'
#' A warning is triggered if the number of folds is greater than the number of
#' elements of any class.
#'
#' @param classes an array of classes
#' @param folds.num the number of folds
#'
#' @return an array, same length as \code{classes}, of numbers in the 1:\code{folds.num} set
#' @export
#'
#' @examples
#' #10 elements of class 1, 20 of class 2 and 3
#' cl = c(rep('class1', 10), rep('class2', 20), rep('class3', 20))
#' stratified_crossvalidation_folds(cl, 5)
#' \dontrun{
#' #this would trigger a waring, since class 1 has only 10 elements
#' stratified_crossvalidation_folds(cl, 15)
#' }
stratified_crossvalidation_folds = function(classes, folds.num){
  res = rep(NA, length(classes))
  #for each different class
  for (c in unique(classes)){
    #where are placed the elements of this class
    where = classes == c

    #how many elements in this class
    n = sum(where)

    #small check: do we have enough elements for the folds
    if (n < folds.num){
      warning(paste('Class', c, 'has less elements than requested folds'))
    }

    #the strata for this class
    str = rep(1:folds.num, length.out = n)

    #shuffling
    str = str[sample(n)]

    #placing them in the original result
    res[where] = str
  }

  return(res)
}
#' Remove markers with low MAF
#'
#' Given a matrix of discrete genotypes (one row per marker, one column per sample),
#' this function removes those markers with a minor allele frequency (MAF) out of the
#' specified bounds (the most common practice is to just filter out for low MAF).
#' If the lower threshold is 0, only fixed markers are
#' removed. Missing values (NAs) are allowed and MAF is accounted accordingly (that is, the
#' specific marker/sample combination is not considered for the computation).
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...ploidy),
#' @param min.MAF the lower MAF threshold, a value between 0 (remove only fixed markers) and 0.5 (no filtering). Test is MAF > min.MAF.
#' @param max.MAF the upper MAF threshold, a value between 0.5 (no filtering) and 0 (only fixed markers are kept). Test is MAF <= max.MAF.
#' @param ploidy the max number of replicates for a specific allele (2=diploid, 4=tetraploid, and so forth)
#'
#' @return a genotype matrix with, usually, less rows
#' @export
filter_on_MAF = function(genotypes, min.MAF = 0.025, max.MAF = 0.5, ploidy = 2){
  alleles.freq = rowSums(genotypes, na.rm = TRUE) / (ploidy * rowSums(!is.na(genotypes)))
  MAF = alleles.freq
  MAF[MAF > 0.5] = 1 - MAF[MAF > 0.5]
  good_markers = (MAF > min.MAF) & (MAF <= max.MAF)

  #limit case need to be tested
  if (sum(good_markers) == 0){
    #filtered too hard...
    return(NULL)
  }

  #otherwise, straightforward filtering
  return(genotypes[good_markers,])
}

#' Remove duplicate markers
#'
#' Given a matrix of discrete genotypes (one row per marker, one column per sample),
#' this function removes those markers that are exact duplicates of others (that is, markers
#' that assume exacty the same values along all samples). Optionally,
#' markers in exact counter phase are also removed (e.g. 0010221 is counter phase to 2212001 for
#' diploids)
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...ploidy),
#' @param remove_counterphase boolean, if TRUE counterphase are removed.
#' @param ploidy the max number of replicates for a specific allele (2=diploid, 4=tetraploid, and so forth). Necessary for counterphase removal
#'
#' @return a genotype matrix with, usually, less rows
#' @export
filter_on_duplicates = function(genotypes, remove_counterphase = FALSE, ploidy = 2){
  #removing duplicated rows
  genotypes = unique(genotypes)

  keep = rep(TRUE, nrow(genotypes))
  if(remove_counterphase){
    #keeping track of the removeable markers
    for (i in 1:(length(keep)-1)){
      #no need to recheck
      if (!keep[i]) next

      #storing the counterphase of the current marker
      current.cntph = ploidy - genotypes[i,]

      #comparing current markers to all others (the unckeded ones)
      for (j in (i+1):length(keep)){
        keep[j] = keep[j] & !all(current.cntph == genotypes[j,])
      }
    }
  }

  return(genotypes[keep,])
}


#' Remove markers that are always heterozygous
#'
#' Markers that are always heterozygous on a population do
#' not bring information (they contain no variability).
#' A fixed heterozygous marker can happen for two reasons:
#' \itemize{
#' \item the population is a cross, each parent was (opposite) homozygous for that marker (parents: AA, aa; offspring: Aa)
#' \item sequencing errors: what is considered "a marker" is actually picking up genotypes from two (or more) loci
#' }
#' In practical terms this function eliminates all markers which
#' have a het frequency is equal or greater than the passed threshold (defaults at 100%, only fixed markers are removed).
#' Het frequency is computed counting, given a marker, the number of het genotypes (genotypes which are
#' not zero or \code{ploidy}) divided by the number of samples.
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...\code{ploidy}),
#' @param ploidy the max number of replicates for a specific allele (2=diploid, 4=tetraploid, and so forth)
#' @param threshold the threshold to start discarding, between 0 and 1. Defaults to 1, meaning only markers fully heterozygous are discarded
#'
#' @return A genotype matrix with, usually, less rows
#' @export
filter_fixed_heterozygous = function(genotypes, ploidy = 2, threshold = 1){
  #if ploidy is not a positive integer we make some fuss
  if (!(ploidy %in% 1:100)){
    stop(paste('Passed ploidy is not a valid integer:', ploidy))
  }

  #het genotypes (i.e. not homozygous)
  het = (genotypes != 0) & (genotypes != ploidy)
  het.freq = rowSums(het, na.rm = TRUE) / rowSums(!is.na(het))

  #finding good markers
  good.markers = het.freq < threshold

  #and we are done
  return(genotypes[good.markers,])
}

#' Remove markers that are (almost) fixed
#'
#' This function removes those markers that contains little or no
#' variation. The check is done for every marker independently.
#' If, given a marker, one of the possible values represents more than
#' \code{threshold} fraction of the total, the marker is removed. E.g.
#' with \code{threshold = 0.95} (the default) a marker is removed if
#' it is fixed (always the same value over all samples) or quasi fixed
#' (up to 95\%).\cr
#' NOTE: missing values are ignored. So a marker [0,0,NA,0] is considered 100\% zeros.
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in [0...ploidy])
#' @param threshold the maximum allowed fraction of samples a value can cover to accept the marker
#'
#' @return a genotype matrix with, usually, less rows
#' @export
filter_fixed_markers = function(genotypes, threshold = 0.95){
  #threshold validation
  if (!is.numeric(threshold)){
    stop(paste('Passed threshold is not numeric'))
  }
  if (threshold > 1 | threshold < 0){
    stop(paste('Passed threshold must be in [0...1] range, passed value:', threshold))
  }

  #for each marker let's find the value showing the maximum number of times
  ma = apply(genotypes, MARGIN = 1, FUN = function(x) max(table(x)))
  samples.no.NA = rowSums(!is.na(genotypes))
  #actual fixation  rate
  fr = ma / samples.no.NA
  markers.good = fr <= threshold

  #when filtering, protect the dimension against a single marker remaining
  samplenames = colnames(genotypes)
  markernames = rownames(genotypes)[markers.good]
  genotypes = genotypes[markers.good,]
  dim(genotypes) = c(length(markernames), length(samplenames))
  rownames(genotypes) = markernames
  colnames(genotypes) = samplenames

  return(genotypes)
}

#' Remove and samples with high missing rate
#'
#' Given a matrix of discrete genotypes (one row per marker, one column per sample),
#' this function removes those markers (and THEN those samples) with a missing
#' rate larger than the specified thresholds.
#' In case of no markers passing the thresholds NULL is returned and a warning is raised.
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...ploidy),
#' @param max_mpm maximum allowed missing per marker rate
#' @param max_mps maximum allowed missing per sample rate
#'
#' @return same data passed via genotypes argumend but with, usually, less rows and columns
#' @export
filter_on_missing_rate = function(genotypes, max_mpm = 0.3, max_mps = 0.3){
  #filter on markers
  missing = is.na(genotypes)
  mpm = rowSums(missing) / ncol(missing)
  markers.good = mpm <= max_mpm

  #if nothing is left, we leave
  if (sum(markers.good) == 0){
    warning('No markers passed the threshold, returning NULL')
    return(NULL)
  }

  #when filtering, protect the dimension against a single marker remaining
  genotypes = genotypes[markers.good,, drop=TRUE]

  #filter on samples
  missing = is.na(genotypes)
  mps = colSums(missing) / nrow(missing)
  samples.good = mps <= max_mps

  #if nothing left, we leave
  if (sum(samples.good) == 0){
    warning('No samples passed the threshold, returning NULL')
    return(NULL)
  }

  #when filtering, protect the dimension against a single sample remaining
  genotypes = genotypes[,samples.good, drop=TRUE]

  return(genotypes)
}

#' Filter SNP matrix based on maximum heterosis
#'
#' This function first removes markers above the specified maximum heterosis,
#' then it removes the samples above the specified level.
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...\code{ploidy}),
#' @param maxhet_marker maximum allowed heterosis level per marker. Alternative to \code{maxsigma_marker} (one of the two must be NULL)
#' @param maxhet_sample maximum allowed heterosis level per sample. Alternative to \code{maxsigma_sample} (one of the two must be NULL)
#' @param maxsigma_marker maximum number of standard deviations from the average heterosis a marker is allowed to have. Alternative to \code{maxhet_marker} (one of the two must be NULL)
#' @param maxsigma_sample maximum number of standard deviations from the average heterosis a sample is allowed to have. Alternative to \code{maxhet_sample} (one of the two must be NULL)
#'
#' @return same data passed via \code{genotypes} argument but with, usually, less rows and columns
#' @export
filter_on_heterosis = function(genotypes, ploidy = 2,
                               maxhet_marker = 0.5, maxhet_sample = 1,
                               maxsigma_marker = NULL, maxsigma_sample = NULL){
  #check on arguments
  if (!xor(is.null(maxhet_marker), is.null(maxsigma_marker))){
    stop('Only one argument admitted, either maxhet_marker or maxsigma_marker')
  }
  if (!xor(is.null(maxhet_sample), is.null(maxsigma_sample))){
    stop('Only one argument admitted, either maxhet_sample or maxsigma_sample')
  }

  #filter on markers
  het = heterosis(genotypes, ploidy = ploidy)
  if (!is.null(maxhet_marker)){
    #filter by fixed threshold
    markers.good = het$by_marker <= maxhet_marker
  }else{
    #filter by outliers
    m = mean(het$by_marker)
    s = sd(het$by_marker)
    markers.good = (het$by_marker <= m + maxsigma_marker * s)
  }

  #if nothing is left, we leave
  if (sum(markers.good, na.rm = TRUE) == 0){
    warning('No markers passed the threshold, returning NULL')
    return(NULL)
  }

  #when filtering, protect the dimension against a single marker remaining
  genotypes = genotypes[markers.good,, drop = FALSE]

  #filter on samples, computing het again
  het = heterosis(genotypes, ploidy = ploidy)
  if (!is.null(maxhet_sample)){
    #filter by fixed threshold
    samples.good = het$by_sample <= maxhet_sample
  }else{
    #filter by outliers
    m = mean(het$by_sample)
    s = sd(het$by_sample)
    samples.good = (het$by_sample <= m + maxsigma_sample * s)
  }

  #if nothing left, we leave
  if (sum(samples.good) == 0){
    warning('No samples passed the threshold, returning NULL')
    return(NULL)
  }

  #when filtering, protect the dimension against a single sample remaining
  genotypes = genotypes[,samples.good, drop = FALSE]

  #done
  return(genotypes)
}


#' Get info on missing per marker and per sample
#'
#' Given a matrix of discrete genotypes (one row per marker, one column per sample),
#' this function computes and returns the missing rates per marker and per sample.
#'
#' @param genotypes a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...ploidy),
#'
#' @return a list with two numeric arrays of missingness
#' @export
missingness = function(genotypes){
  missing = is.na(genotypes)

  mpm = rowSums(missing) / ncol(missing)
  names(mpm) = rownames(genotypes)

  mps = colSums(missing) / nrow(missing)
  names(mps) = colnames(genotypes)

  return(list(
    per.marker = mpm,
    per.sample = mps
  ))
}

#' Compute heterosis
#'
#' This function computes heterosis on a per-sample and per-marker basis, other
#' than the total.
#'
#' @param SNP a matrix of discrete genotypes (one row per marker, one column per sample, values in 0...ploidy),
#' @param ploidy integer, the maximum value (theoretically) found in SNP matrix
#'
#' @return a list with $by_sample, $by_marker and $total fields
#' @export
heterosis = function(SNP, ploidy = 2){
  res = list()

  #computing heterosis
  het_loci = SNP != 0 & SNP != ploidy
  res$by_sample = colMeans(het_loci, na.rm = TRUE)
  res$by_marker = rowMeans(het_loci, na.rm = TRUE)
  res$total = sum(het_loci, na.rm = TRUE) / (nrow(het_loci) * ncol(het_loci))

  return(res)
}

#' Compute correlation by strata
#'
#' Given two sets of data (two one-dimensional arrays) computes the correlation by strata.
#'
#' @param x numeric array, first data set
#' @param y numeric array, second data set
#' @param strata array giving the data partitioning in strata
#' @param ... each other parameter is passed to \code{\link[stats]{cor}} function
#'
#' @return a named array of correlations
#' @export
#'
#' @examples
#' #two random, completely uncorrelated arrays
#' a = runif(100)
#' b = runif(100)
#' #the first half gets a bias
#' a[1:50] = a[1:50] + 10
#' b[1:50] = b[1:50] + 10
#' #standard correlation, pretty high because of bias
#' cor(a, b)
#' #stratified correlation inside each stratum is pretty low
#' strata = c(rep('first', 50), rep('second', 50))
#' stratified_correlation(a, b, strata)
stratified_correlation = function(x, y, strata, ...){
  res = c()
  for (s in unique(strata)){
    sel.s = strata == s
    res[s] = cor(x[sel.s], y[sel.s], ...)
  }
  return(res)
}


#' Builds a confusion matrix
#'
#' Given two same-dimension arrays (one for true classes, one for predicted classes)
#' this function returns a confusion matrix. Rows correspond to true values, columns
#' to predicted values. The main diagonal thus reports the correct predictions. Out of
#' diagonal elements explain for each class what is confounded.
#'
#' @param true_vals an array of true classes
#' @param predicted_vals an array of predicted classes
#'
#' @return a square confusion matrix, rows are true values, columns are predicted values
#' @export
confusion_matrix = function(true_vals, predicted_vals){
  #first of all, to avoid numeric problems, we move to characters
  true_vals = as.character(true_vals)
  predicted_vals = as.character(predicted_vals)

  #retrieving the list of all classes
  classes = sort(union(unique(true_vals), unique(predicted_vals)))

  #preparing the matrix
  res = matrix(data=NA, nrow = length(classes), ncol=length(classes))
  colnames(res) = classes
  rownames(res) = classes

  #filling the matrix
  for (true_curr in classes){
    for(pred_curr in classes){
      res[true_curr, pred_curr] = sum(true_vals == true_curr & predicted_vals == pred_curr)
    }
  }

  return(res)
}

#' Creates a string representation of a parameter array
#'
#' A string representation of the passed array is returned, so that it can be used for
#' file names and easy identification. If \code{x} is named,
#' slot names are used in the slug, otherwise a param1, param2, ... numeration is used.
#' With default separators the slug will take the form param1-value1_param2-value2 and so forth.
#'
#' @param x an array to be transformed in slug
#' @param separator1 the parameter/parameter separator
#' @param separator2 the parameter_name/value separator
#'
#' @return a string representation of a parameter array
#' @export
#'
#' @examples
#' #named array
#' a = c(this = 10, named = 20, array = 30)
#' slug(a) #returns: "this-10_named-20_array-30"
#' #nameless array
#' b = 1:5
#' slug(b) #returns: "param1-1_param2-2_param3-3_param4-4_param5-5"
slug = function(x, separator1 = '_', separator2 = '-'){
  #nameless array go to empty names
  if (is.null(names(x))){
    names(x) = rep('', length(x))
  }

  #building the slug
  res = NULL
  for (i in 1:length(x)){
    #current param name
    name.curr = names(x[i])
    if (name.curr == ''){
      #nameless parameter
      name.curr = paste(sep='', 'param', i)
    }

    #current param-value
    slug.curr =  paste(sep=separator2, name.curr, x[i])

    #updating the slug
    if (is.null(res)){
      res = paste(sep=separator1, slug.curr)
    }else{
      res = paste(sep=separator1, res, slug.curr)
    }
  }

  return(res)
}


#' Estimates the number of columns in a text matrix file
#'
#' This function reads the first line of the (possibly .gz) passed file
#' and estimates how many chuncks are there, using the passed separator.
#'
#' @param filename the file (text file or .gz compressed text file) to be examined
#' @param sep the separator for columns
#'
#' @return the number of columns in the passed file
#' @export
estimate_ncols = function(filename, sep='\t'){
  l = readLines(filename, n = 1)
  l = strsplit(l, split = sep)[[1]]
  return(length(l))
}

#' Transposes a big file without loading it in memory
#'
#' The passed infile is read by chuncks of nlines and transposed. It never gets
#' loaded in memory in its entirety. However, it requires space on permanent memory
#' for the final transposed version and for the copy of all the chuncks (effectively
#' it requires two times the size of the file as free space). The function transparently
#' supports .gz compression of inputs and outputs.
#'
#' @param infile the file to be transposed
#' @param outfile the name of the new, transposed version
#' @param tmpdir optional folder to be used for temporarary files
#' @param nlines the number of lines of each chunck of infile
#' @param sep the separator used to split columns
#' @param ncols if known, the number of columns of infile
#'
#' @return nothing, the file are saved on permanent memory
#' @export
transpose_big_file = function(infile, outfile = paste(sep='.', infile, 'transposed'), tmpdir = tempdir(), nlines = 10, sep='\t', ncols = estimate_ncols(infile)){
  #transparently supporting gz files
  is_gz = substr(infile, start = nchar(infile)-2, stop=nchar(infile)) == '.gz'
  if (is_gz){
    fp = gzfile(infile, open = 'r')
  }else{
    fp = file(infile, open = 'r')
  }

  #keeping note of all the temp files
  tmp_files = NULL

  while(TRUE){
    #trying to read a chunk of data
    chunk = t(matrix(
      data = scan(file=fp, what=character(), sep = sep, nlines = nlines, quiet = TRUE),
      ncol = ncols,
      byrow = TRUE
    ))

    #if we don't get anything we stop
    if (ncol(chunk) == 0){
      break()
    }

    #creating a tmp file for this chunk
    tmp_curr = tempfile(tmpdir = tmpdir)
    tmp_files = c(tmp_files, tmp_curr)

    #saving there the uncompressed data
    write.table(chunk, file = tmp_curr, sep=sep, quote=FALSE, row.names = FALSE, col.names = FALSE)
  }

  #assemblying with the paste utility
  final_tmp = tempfile(tmpdir = tmpdir)
  system2('paste', c(tmp_files, '>', final_tmp), stdout = TRUE, stderr = TRUE)

  #if required, wewe compress output too
  is_gz = substr(outfile, start = nchar(outfile)-2, stop=nchar(outfile)) == '.gz'
  if (is_gz){
    system2('gzip', final_tmp, stdout = TRUE, stderr = TRUE)

    #gzipping should have removed the final file, but just in case
    if (file.exists(final_tmp)){
      file.remove(final_tmp)
    }

    #we now consider only the gzipped version
    final_tmp = paste(sep='', final_tmp, '.gz')
  }

  #and moving to the final destination
  system2('mv', c(final_tmp, outfile), stdout = TRUE, stderr = TRUE)

  #removing the temp files
  for (i in 1:length(tmp_files)){
    file.remove(tmp_files[i])
  }

  #closing the data in
  close(fp)
}
