# install.packages("devtools")
# devtools::install_github("jendelman/GWASpoly", build_vignettes=FALSE)
library(GWASpoly)
library(stringi)
library(statgenGWAS)

#' WGBLUP using BGRL
#'
#' This function computes a weighted kinship matrix GBLUP as described in
#' "Weighted Genomic Best Linear Unbiased Prediction for Carcass Traits in Hanwoo Cattle"
#' by Lopez et al., 2019.
#'
#' @param phenotypes phenotypes, a numeric array (n x 1), missing values are predicted. Marker names should be in chromosome_position format
#' @param genotypes SNP genotypes, one row per phenotype (n), one column per marker (m), values in 0/1/2 for
#'                  diploids or 0/1/2/...ploidy for polyploids. Can be NULL if \code{covariances} is present.
#' @param covariances not supported, but kept with compatibility with GROAN. If not NULL an error is risen
#' @param extraCovariates extra covariates set, one row per phenotype (n), one column per covariate (w).
#'                 If NULL no extra covariates are considered.
#' @param LOCO passed to \code{\link[madbito]{getWeights_GWASpoly}}
#' @param nIter BGLR param, with a set default
#' @param burnIn BGLR param, with a set default
#' @param SNP_pos dataframe, rowsnames are SNP names, columns are "chrom" (chromosom name) and "bp" (position of the marker in base pairs)
#' @param ... extra parameters are passed to \code{\link[BGLR]{BGLR}}
#'
#' @return The function returns a list with the following fields:
#' \itemize{
#'   \item \code{predictions} : an array of (n) predicted phenotypes, with NAs filled and all other positions repredicted (useful for calculating residuals)
#'   \item \code{hyperparams} : empty, returned for compatibility
#'   \item \code{extradata}   : list with information on trained model, coming from \code{\link[BGLR]{BGLR}}
#' }
#' @export
phenoRegressor.WGBLUP = function(phenotypes, genotypes, covariances = NULL, extraCovariates = NULL, LOCO = TRUE, nIter = 10000, burnIn = 1000, SNP_pos, ...){
  #no support for covariances
  stopifnot(is.null(covariances))

  #selecting only complete data
  sel = !is.na(phenotypes)

  #computing SNP weights via association study. Some SNP may be removed in
  #the process (i.e. monomorphic SNPs) so we need to take that into account later
  SNP_weights = getWeights_GWASpoly(SNP = genotypes[sel,, drop = FALSE],
                                    pheno_values = phenotypes[sel, drop = FALSE],
                                    pheno_name = 'placeholder', toy_data = FALSE, LOCO = LOCO,
                                    SNP_pos = SNP_pos)
  #same SNPs, same order
  stopifnot(all(SNP_weights$snp %in% colnames(genotypes)))
  genotypes = genotypes[, SNP_weights$snp, drop=FALSE]

  # Gstar
  Gstar = build_Gstar(weights = SNP_weights$logP, SNP = genotypes, toy_data = FALSE)

  #RKHS based GBLUP
  preds = phenoRegressor.BGLR(
    phenotypes=phenotypes,
    covariances = Gstar,
    extraCovariates = extraCovariates,
    type = 'RKHS', nIter = nIter, burnIn = burnIn, ...)

  #and we are done
  return(preds)
}

#' SNP weights from GWAS
#'
#' A step in implementing WGBLUP, we need weights for each marker
#'
#' @param SNP marker matrix, samples in rows, markers in columns. Marker names should be in chromosome_position format
#' @param pheno_values one value per sample in the SNP matrix
#' @param pheno_name how to call the phenotype
#' @param LOCO boolean, should be TRUE unless very fragmented reference genome
#' @param toy_data if TRUE only the first 1000 markers are used
#' @param ploidy ploidy passed to GWASpoly
#' @param SNP_pos dataframe, rowsnames are SNP names, columns are "chrom" (chromosom name) and "bp" (position of the marker in base pairs)
#'
#' @return result of statgenGWAS analysis
getWeights_GWASpoly = function(SNP, pheno_values, pheno_name, LOCO, toy_data = FALSE, ploidy = 2, SNP_pos){
  #building the GWASpoly phenotype structure
  GWASPOLY_phenos = data.frame(
    GEN = rownames(SNP),
    phenotype = pheno_values
  )
  colnames(GWASPOLY_phenos) = c('GEN', pheno_name)

  #building the GWASpoly genotype structure
  SNP = t(SNP)

  #if required, we subset to only 1000 SNPs
  if (toy_data){
    SNP = SNP[1:1000,]
  }

  #retrieving positions and chroms, then building the actual data structure
  GWASPOLY_genos = data.frame(
    "marker" = rownames(SNP),
    SNP_pos[rownames(SNP), c('chrom', 'bp')],
    SNP
  )

  #saving and then reading... ARGH
  tmp_genofile = tempfile(pattern = 'geno')
  tmp_phenofile = tempfile(pattern = 'pheno')
  write.csv(GWASPOLY_phenos, file = tmp_phenofile, row.names = FALSE)
  write.csv(GWASPOLY_genos, file = tmp_genofile, row.names = FALSE)
  mydata = GWASpoly::read.GWASpoly(ploidy = ploidy, pheno.file = tmp_phenofile, geno.file = tmp_genofile, format = 'numeric', n.traits = 1, delim = ',')

  #cleanup
  file.remove(tmp_genofile)
  file.remove(tmp_phenofile)

  #actual association scores
  writeLines(paste('    * computing K'))
  myK = GWASpoly::set.K(data = mydata, LOCO = LOCO)
  writeLines(paste('    * computing scores'))

  #in case of polyploids we use GWASpoly
  if (ploidy > 2){
    scores = GWASpoly::GWASpoly(data = myK, models = 'additive')
    scores = scores@scores[[pheno_name]]

    #this is added for compatiiblity
    scores$snp = rownames(scores)
    colnames(scores) = c('logP', 'snp')
  }else{
    scores = getScores_statgen(myK)
  }

  #and we are done
  return(scores)
}

#internal function, used by getWeights_GWASpoly
getScores_statgen = function(GWASpoly_K){
  #preparing data shapes and names
  tmp_pheno = col.rename(x = GWASpoly_K@pheno, oldname = 'GEN', newname = 'genotype')
  tmp_map = GWASpoly_K@map
  tmp_map = col.rename(tmp_map, oldname = 'Chrom', newname = 'chr')
  tmp_map = col.rename(tmp_map, oldname = 'Position', newname = 'pos')
  rownames(tmp_map) = tmp_map$Marker

  #creating statgen GWAS data struct
  myGdata = statgenGWAS::createGData(
    geno = GWASpoly_K@geno,
    kin = GWASpoly_K@K$all,
    pheno = tmp_pheno,
    map = tmp_map
  )

  #computing scores
  scores = statgenGWAS::runSingleTraitGwas(gData = myGdata)

  #adding the logP
  scores = scores$GWAResult$tmp_pheno
  scores$logP = -log10(scores$pValue)

  #done
  return(data.frame(scores))
}


#' Computed Gstar matrix for WGBLUP regression
#'
#' Given weights and SNPs, returns the Gstar matrix
#'
#' @param weights weights for SNPs
#' @param SNP marker matrix, samples in rows, SNPs in columns
#' @param toy_data if TRUE only the first 1000 markers are used
#' @param ploidy genotype ploidy, used for computing MAF
#'
#' @return the Gstar matrix
build_Gstar = function(weights, SNP, toy_data = FALSE, ploidy = 2){
  #should we subset?
  if (toy_data){
    SNP = SNP[, 1:1000]
  }

  sel = !is.na(weights)
  if (sum(!sel) > 0){
    warning(paste('Removed', sum(!sel), 'markers for not having a well defined weight'))
    SNP = SNP[,sel]
    weights = weights[sel]
  }
  Z = SNP

  #compute MAF
  MAF = colSums(Z) / (ploidy*nrow(Z))
  sel = MAF >= 0.5
  MAF[sel] = 1 - MAF[sel]

  #compute G*
  # G* = ZDZ' / (2 * sum (MAF * (1-MAF)))
  den = 2 * sum(MAF * (1-MAF))

  #D = diag(weights) -> too big, it becomes SNP x SNP
  #D2 = weights, repeated for all rows, then element by element product
  D2 = matrix(rep(weights, nrow(Z)), nrow = nrow(Z), byrow = TRUE)
  Gstar = Z * D2
  Gstar = (as.matrix(Gstar) %*% t(SNP)) / den

  #done
  return(Gstar)
}
